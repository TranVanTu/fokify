import Search from './models/Search';
import * as searchView from './views/searchView';
import * as recipeView from './views/recipeView';
import { DOMelements, renderLoader, clearLoader } from './views/base';
import Recipe from './models/Recipe';
import List from './models/List';
import * as listView from './views/listView';
import Likes from './models/Likes';
import * as likeView from './views/likesView';
/*
  global state of the app
   - search object
   - current recipe object
   - shopping list object
   - liked object
*/
const state = {};

const controlSearch = async () => {
  //  step 1 : get query from the view
  const query = searchView.getInput();
  if (query)
  {
    // step 2 : new search object and add to state
    state.search = new Search(query);
    // step 3 : prepare UI for the results
    searchView.clearInput();
    searchView.clearResults();
    renderLoader(DOMelements.resultsContainer);
    // step 4 : search for recipes
    try
    {
      await state.search.getResults();

      // step 5 : render results on UI
      searchView.renderResults(state.search.results);

      //  step 6 : clear loader
      clearLoader();
    } catch (err)
    {
      alert('error processing search');
      clearLoader();
    }
  } else
  {
    // 
    console.log('The query is not mathced any meal');
  }

}
DOMelements.searchForm.addEventListener('submit', function (e) {
  e.preventDefault();
  controlSearch();
});

DOMelements.searchResPages.addEventListener('click', (event) => {
  const btn = event.target.closest('.btn-inline');
  if (btn)
  {
    const gotoPage = parseInt(btn.dataset.goto, 10);
    searchView.clearResults();
    searchView.renderResults(state.search.results, gotoPage);
  }
});

const controlRecipe = async () => {
  //get ID from url
  const id = window.location.hash.replace('#', '');
  // render recipe
  if (id)
  {
    // prepare UI for changes
    renderLoader(DOMelements.recipe);
    // highlight selected recipe
    if (state.search)
    {
      searchView.highlightedSelected(id);
    }
    // create new recipe object
    state.recipe = new Recipe(id);
    try
    {
      // get recipe data and parse ingredients
      await state.recipe.getRecipe();
      state.recipe.parseIngredients();
      // // calculate servings and time
      state.recipe.calcTime();
      state.recipe.calcServings();
      // render recipe
      clearLoader();
      recipeView.renderRecipe(state.recipe, state.likes.isLiked(id));
    } catch (err)
    {
      console.log(err);
      console.log('Error processing recipe!!!');
    }
  }
}
// recipe controller
['hashchange', 'load'].forEach(event => {
  window.addEventListener(event, controlRecipe);
});

const controlList = () => {
  // create a new list if there is no yet
  if (!state.list)
  {
    state.list = new List();
  }
  // add each ingredient to the list and UI
  state.recipe.ingredients.forEach(ing => {
    const item = state.list.addItem(ing.count, ing.unit, ing.ingredient);
    listView.renderItem(item);
  });
}
state.likes = new Likes();
likeView.toggleLikeMenu(state.likes.getNumLikes());
// like controller
const controlLikes = () => {
  if (!state.likes)
  {
    state.likes = new Likes();
  }
  const curId = state.recipe.id;
  // user has not liked the current recipe
  if (!state.likes.isLiked(curId))
  {
    // add like to the state
    const newLike = state.likes.addLike(
      curId,
      state.recipe.title,
      state.recipe.author,
      state.recipe.image_url
    );
    // toggle like button
    likeView.toggleLikeBtn(true);
    // add like to the UI list
    likeView.renderLike(newLike);
  }
  // user has liked the current recipe
  else
  {
    // remove like from the state
    state.likes.deleteLike(curId);
    // toggle like button
    likeView.toggleLikeBtn(false);
    // remove like from the UI list
    likeView.deleteLike(curId);
  }
  likeView.toggleLikeMenu(state.likes.getNumLikes());
};

// handle delete and update list item events

DOMelements.shopping.addEventListener('click', (event) => {
  const id = event.target.closest('.shopping__item').dataset.itemid;
  // handle delete btn
  if (event.target.matches('.shopping__delete, .shopping__delete *'))
  {
    // delete from state
    state.list.deleteItem(id);

    // delete from UI
    listView.deleteItem(id);
  } else if (event.target.matches('.shopping__count-value'))
  {
    const val = parseFloat(event.target.value);
    state.list.updateCount(id, val);
  }

});


// handling recipe serving button click

DOMelements.recipe.addEventListener('click', (event) => {
  if (event.target.matches('.btn-decrease ,.btn-decrease *'))
  {
    // decrease btn was clicked
    if (state.recipe.servings > 1)
    {
      state.recipe.updateServings('dec');

    }
    recipeView.renderRecipe(state.recipe);

  } else if (event.target.matches('.btn-increase ,.btn-increase *'))
  {
    // increase btn was clicked
    if (state.recipe.servings < 50)
    {
      state.recipe.updateServings('inc');
    }
    recipeView.renderRecipe(state.recipe);
  } else if (event.target.matches('.add__shopping-cart, .add__shopping-cart *'))
  {
    // add to shopping cart
    controlList();
  } else if (event.target.matches('.recipe__love, .recipe__love *'))
  {
    // control like list
    controlLikes();
    // 
  }
});

