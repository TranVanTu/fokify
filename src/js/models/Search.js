import axios from '../../../node_modules/axios';
export default class Search {
  constructor(query) {
    this.query = query;
  }
  async getResults() {
    // const proxy = 'https://crossorigin.me/'; // do not need now because we are using other api with no key or proxy
    try
    {
      const res = await axios(`https://forkify-api.herokuapp.com/api/search?q=${this.query}`);
      this.results = res.data.recipes;
    } catch (error)
    {
      alert(error);
    }
  }
}

