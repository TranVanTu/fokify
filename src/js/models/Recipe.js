import axios from '../../../node_modules/axios';

export default class Recipe {
  constructor(id) {
    this.id = id;
  }
  async getRecipe() {
    try
    {
      const res = await axios(`https://forkify-api.herokuapp.com/api/get?rId=${this.id}`);
      this.author = res.data.recipe.publisher;
      this.image_url = res.data.recipe.image_url;
      this.title = res.data.recipe.title;
      this.ingredients = res.data.recipe.ingredients;
      this.url = res.data.recipe.source_url;
    } catch (err)
    {
      console.log(err);
      alert('Something go wrong');
    }

  }
  calcTime() {
    // assume we need 15 minutes for a meal completed
    const numIng = this.ingredients.length;
    const periods = Math.ceil(numIng / 3);
    this.time = periods * 15;
  }
  calcServings() {
    this.servings = 4;
  }
  parseIngredients() {
    const unitLong = ['tablespoons', 'tablespoon', 'ounce', 'ounces', 'teaspoon', 'teaspoons', 'cups', 'pounds'];
    const unitShort = ['tbsp', 'tbsp', 'oz', 'oz', 'tsp', 'tsp', 'cup', 'pound'];
    const units = [...unitShort, 'kg', 'g'];
    const newIngredients = this.ingredients.map(ing => {

      // 1. uniform units

      let ingredient = ing.toLowerCase();
      unitLong.forEach((unit, index) => {
        ingredient.replace(unit, units[index]);
      });

      // 2. remove parenthesis
      ingredient = ingredient.replace(/ *\([^)]*\) */g, " ");
      // 3. parse ingredients into count, unit and ingredient
      const arrIng = ingredient.split(' ');
      const unitIndex = arrIng.findIndex(el => unitShort.includes(el));
      let objIng;
      if (unitIndex > -1)
      {
        //  There is a unit
        const arrCount = arrIng.slice(0, unitIndex);
        let count;
        if (arrCount.length === 1)
        {
          count = eval(arrCount[0].replace('-', '+'));
        } else
        {
          count = eval(arrIng.slice(0, unitIndex).join('+'));
        }
        objIng = {
          count: count,
          unit: arrIng[unitIndex],
          ingredient: arrIng.slice(unitIndex + 1, arrIng.length).join(' ')
        };
      }
      else if (parseInt(arrIng[0], 10))
      {
        // there is no unit but 1st element is number
        objIng = {
          count: parseInt(arrIng[0], 10),
          unit: '',
          ingredient: arrIng.slice(1).join(' ')
        }
      } else if (unitIndex === -1)
      {
        //  There is no unit and no number at the 1st position
        objIng = {
          count: 1,
          unit: '',
          ingredient: ingredient
        }
      }
      return objIng;
    });

    this.ingredients = newIngredients;
  }
  updateServings(type) {
    // new servings people 

    const newServings = type === 'dec' ? this.servings - 1 : this.servings + 1;

    // ingredients
    this.ingredients.forEach(ing => {
      ing.count = ing.count * (newServings / this.servings);
    });
    this.servings = newServings;

  }
}