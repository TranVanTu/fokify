import { DOMelements } from './base';
export const getInput = () => DOMelements.searchInput.value;

export const clearInput = () => {
  DOMelements.searchInput.value = '';
};
export const clearResults = () => {
  DOMelements.searchResultsList.innerHTML = '';
  DOMelements.searchResPages.innerHTML = '';
}
export const limitRecipeTitle = (title, limit) => {
  const newTitle = [];
  if (title.length > limit)
  {
    title.split(' ').reduce((acc, cur) => {
      if (acc + cur.length <= limit)
      {
        newTitle.push(cur);
      }
      return acc + cur.length;
    }, 0);

    // return the results
    return `${newTitle.join('')}...`;
  } else
  {
    return title;
  }
};
export const highlightedSelected = id => {
  const results = Array.from(document.querySelectorAll('.results__link'));
  results.forEach(result => {
    result.classList.remove('results__link--active');
  });
  document.querySelector(`.results__link[href='#${id}']`).classList.add('results__link--active');
};

const renderRecipe = recipe => {
  const markkup = `
    <li>
      <a class="results__link" href="#${recipe.recipe_id}">
          <figure class="results__fig">
                <img src="${recipe.image_url}" alt="Test">
            </figure>
          <div class="results__data">
            <h4 class="results__name">${limitRecipeTitle(recipe.title, 17)}</h4>
            <p class="results__author">${recipe.publisher}</p>
          </div>
      </a>
    </li>
  `;
  DOMelements.searchResultsList.insertAdjacentHTML('beforeend', markkup);
};
// type prev or next

const createButtons = (page, type) => `
  <button class="btn-inline results__btn--${type}" data-goto=${type === 'prev' ? page - 1 : page + 1}>
    <svg class="search__icon">
        <use href="img/icons.svg#icon-triangle-${type === 'prev' ? 'left' : 'right'}"></use>
    </svg>
    <span>Page ${type === 'prev' ? page - 1 : page + 1}</span>
  </button>`;

const renderButtons = (page, numResults, resPerPage) => {
  const pages = Math.ceil(numResults / resPerPage);
  let button;
  if (page === 1 && pages > 1)
  {
    //  button go to the next page
    button = createButtons(page, 'next');
  } else if (page === pages && pages > 1)
  {
    // button to go to previous page
    button = createButtons(page, 'prev');
  } else if (page > 1 && page < pages)
  {
    //  display both button
    button = `
      ${createButtons(page, 'prev')}
      ${createButtons(page, 'next')}
    `
  }
  DOMelements.searchResPages.insertAdjacentHTML('afterbegin', button);
};

export const renderResults = (recipes, page = 1, resPerPage = 10) => {
  // render result of current page
  const start = (page - 1) * resPerPage;
  const end = page * resPerPage;
  recipes.slice(start, end).forEach(recipe => renderRecipe(recipe));
  // render the pagination buttons
  renderButtons(page, recipes.length, resPerPage)
};