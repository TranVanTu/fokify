import { setTimeout } from "core-js";

export const DOMelements = {
  searchForm: document.querySelector('.search'),
  searchInput: document.querySelector('.search__field'),
  searchResultsList: document.querySelector('.results__list'),
  resultsContainer: document.querySelector('.results'),
  searchResPages: document.querySelector('.results__pages'),
  recipe: document.querySelector('.recipe'),
  shopping: document.querySelector('.shopping__list'),
  likesMenu: document.querySelector('.likes'),
  likeList : document.querySelector('.likes__list')
};
export const elementStr = {
  loader: '.loader'
}
export const renderLoader = parent => {
  const loader = `
    <div class="loader">
      <svg>
        <use href="img/icons.svg#icon-cw" ></use>
      </svg>
    </div>
  `;
  parent.insertAdjacentHTML('afterbegin', loader);
}

export const clearLoader = () => {
  const loader = document.querySelector(elementStr.loader);
  loader.parentNode.removeChild(loader);
};